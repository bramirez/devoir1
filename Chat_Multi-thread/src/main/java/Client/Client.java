package Client;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Class that handle the client
 */
public class Client {
	/**
	 * Main function of the client
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		String pseudoError = "Ce pseudonyme existe deja.";
		Scanner scanner = new Scanner(System.in);

		try {
			Socket client = new Socket("localhost", 1800);
			System.out.println("Vous etes connect�");
			DataInputStream input = new DataInputStream(client.getInputStream());
			DataOutputStream output = new DataOutputStream(client.getOutputStream());

			// Loop if the pseudo is not duplicate thanks to the answer of the server.
			while (pseudoError.equals("Ce pseudonyme existe deja.")) {
				System.out.println("Entrez votre pseudonyme : ");
				String pseudo = scanner.nextLine();
				output.writeUTF(pseudo);
				output.flush();
				pseudoError = input.readLine();
				System.out.println(pseudoError);
			}

			// Thread sending
			ThreadWrite sendMsg = new ThreadWrite(scanner, output, input, client);
			sendMsg.start();

			// Thread reading
			ThreadRead readMsg = new ThreadRead(input, output, client);
			readMsg.start();

		} catch (IOException ex) {
			System.out.println("Erreur lors de la connection au serveur : connection impossible.");
			System.exit(0);
		}
	}

	/**
	 * Class that handle the reading thread for the client
	 */
	public static class ThreadRead extends Thread {
		String msg;
		DataInputStream input;
		DataOutputStream output;
		Socket client;
		/**
		 * Constructor that handle the reading thread for the client
		 *
		 * @param input, output, client
		 */

		public ThreadRead(DataInputStream input, DataOutputStream output, Socket client) {
			this.input = input;
			this.output = output;
			this.client = client;
		}

		/**
		 * Function that handle the server connection
		 *
		 */
		@Override
		public void run() {
			try {
				msg = input.readLine();
				while (msg != null) {
					System.out.println(msg);
					msg = input.readLine();
				}
				System.out.println("Serveur d�connect�");
				output.close();
				client.close();
			} catch (IOException e) {
				System.out.println("Connexion au serveur perdue");
				System.exit(0);
			}
		}

		
	}

	/**
	 * Class that handle the writing thread for the client
	 */
	public static class ThreadWrite extends Thread {
		String msg;
		Scanner scanner;
		DataOutputStream output;
		DataInputStream input;
		Socket client;
		
		/**
		 * Constructor that handle the writing thread for the client
		 *
		 * @param scanner, input, output, client
		 */
		public ThreadWrite(Scanner scanner, DataOutputStream output, DataInputStream input, Socket client) {
			this.scanner = scanner;
			this.output = output;
			this.input = input;
			this.client = client;
		}

		/**
		 * Function that handle the server connection
		 *
		 */
		@Override
		public void run() {
			while (true) {
				msg = scanner.nextLine();
				try {
					if (msg.equals("exit")) {
						input.close();
						output.close();
						client.close();
						System.out.println("--> Fin de connexion, au revoir !");
						System.exit(0);
					}
					output.writeUTF(msg);
					output.flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		

	}

}
