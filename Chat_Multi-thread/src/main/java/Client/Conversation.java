package Client;

import java.io.*;
import java.net.Socket;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import Server.Server;

/**
 * class that listens to one user
 */
public class Conversation extends Thread {
	private Socket socket;
	private String pseudo;
	private PrintWriter output;
	private DataInputStream input;

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	/**
	 * 
	 */
	@Override
	public void run() {
		try {
			DataInputStream input = new DataInputStream(socket.getInputStream());
			while (true) {
				String msg = input.readUTF();
				System.out.println(pseudo + " a dit : " + msg);
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
				LocalDateTime now = LocalDateTime.now();
				Server.broadcastMessage("(" + dtf.format(now) + ") " + pseudo + " a dit : " + msg);
			}
		} catch (IOException e) {
			String msg = "--> L'utilisateur " + pseudo + " a quitte la conversation.";
			System.out.println(msg);
			Server.deleteClient(pseudo);
			Server.broadcastMessage(msg);
		}
	}

	public Conversation(Socket socket, String pseudo, PrintWriter printWriter, DataInputStream input) {
		this.socket = socket;
		this.pseudo = pseudo;
		this.output = printWriter;
		this.input = input;
	}

}