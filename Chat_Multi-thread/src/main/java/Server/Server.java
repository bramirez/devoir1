package Server;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import Client.Conversation;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Class server
 */
public class Server extends Thread {
	private static HashMap<String, Socket> listCLient = new HashMap<String, Socket>();
	private static HashMap<String, Conversation> listThread = new HashMap<String, Conversation>();

	/**
	 * Function that start the server.
	 *
	 * @param args
	 */
	public static void main(String args[]) {
		new Server().start();
	}

	/**
	 * Function that initialize the server and wait for a new client
	 */
	@Override
	public void run() {
		try {
			ServerSocket serverSocket = new ServerSocket(1800);
			System.out.println("Le server est actif");

			// EndLess loop who accepts the incoming requests.
			while (true) {
				Socket socket = serverSocket.accept();
				String pseudo = createCLient(socket);
				Server.listCLient.put(pseudo, socket);
				System.out.println(Server.listCLient.size() + " clients en ce moment.");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Function that create a new client in the list with a valid name. It means no
	 * duplicates names.
	 *
	 * @param socket socket dedicated to the new user
	 * @return pseudo of the new user
	 * @throws IOException
	 */
	private String createCLient(Socket socket) throws IOException {
		PrintWriter printWriter = null;
		DataInputStream data = new DataInputStream(socket.getInputStream());
		String pseudo = data.readUTF();

		printWriter = new PrintWriter(socket.getOutputStream(), true);
		while (Server.listCLient.containsKey(pseudo)) {
			printWriter.println("Ce pseudonyme existe deja.");
			data = new DataInputStream(socket.getInputStream());
			pseudo = data.readUTF();
		}
		System.out.println("--> Nouveau client : " + pseudo);
		Conversation convClient = new Conversation(socket, pseudo, printWriter, data);
		convClient.start();
		Server.listThread.put(pseudo, convClient);
		printWriter.println(pseudo + ", vous avez rejoint la discution.");

		Iterator iterator = Server.listCLient.entrySet().iterator();
		if (iterator.hasNext()) {
			printWriter.println("Clients connectes : ");
		} else {
			printWriter.println("Vous etes seul sur la discussion. ");
		}
		while (iterator.hasNext()) {
			Map.Entry mapentry = (Map.Entry) iterator.next();
			printWriter.println(mapentry.getKey());
		}
		printWriter.println("----------------------------------- ");

		Server.broadcastMessage("--> " + pseudo + " a rejoint la discussion.");
		return pseudo;
	}

	/**
	 * Function that broadcast the message on the others client
	 *
	 * @param message
	 */
	public static void broadcastMessage(String message) {
		listCLient.forEach((key, value) -> {
			PrintWriter printWriter = null;
			try {
				printWriter = new PrintWriter(value.getOutputStream(), true);
			} catch (IOException e) {
				e.printStackTrace();
			}
			printWriter.println(message);
		});
	}

	/**
	 * Function that delete the User in the list of user and thread
	 *
	 * @param pseudo of the user
	 */
	public static void deleteClient(String pseudo) {
		listCLient.remove(pseudo);
		listThread.remove(pseudo);
	}

}
